**THE INDUSTRY REVOLUTION 3.0**

Beginning in the 1950s, the third industrial revolution brought semiconductors, mainframe computing, personal computing, and the Internet—the digital revolution. Things that used to be analog moved to digital technologies, like an old television you used to tune in with an antenna (analog) being replaced by an Internet-connected tablet that lets you stream movies (digital).

The move from analog electronic and mechanical devices to pervasive digital technology dramatically disrupted industries, especially global communications and energy. Electronics and information technology began to automate production and take supply chains global.



![](https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSVdXoNv0QBeI6y0iIXE53DxrSeonZechWHag&usqp=CAU)

**THE INDUSTRY REVOLUTION 4.0**

Industry 4.0 is the digital transformation of manufacturing/production and related industries and value creation processes.

Industry 4.0 is used interchangeably with the fourth industrial revolution and represents a new stage in the organization and control of the industrial value chain.Industry 4.0 is often used interchangeably with the notion of the fourth industrial revolution. It is characterized by, among others, 1) even more automation than in the third industrial revolution, 2) the bridging of the physical and digital world through cyber-physical systems, enabled by Industrial IoT, 3) a shift from a central industrial control system to one where smart products define the production steps, 4) closed-loop data models and control systems and 4) personalization/customization of products.

![](https://40uu5c99f3a2ja7s7miveqgqu-wpengine.netdna-ssl.com/wp-content/uploads/2017/02/Industry-4.0-digital-transformation-of-manufacturing-in-the-fourth-industrial-revolution.gif)


**THE COMMUNICATION PROTOCOLS**


**MQTT**


MQTT (Message Queue Telemetry Transport) is a lightweight protocol for sending simple data flows from sensors to applications and middleware.

The protocol functions on top of TCP/IP and includes three components: subscriber, publisher and broker. The publisher collects data and sends it to subscribers. The broker tests publishers and subscribers, checking their authorization and ensuring security.

MQTT suits small, cheap, low-memory and low-power devices.

**DDS**


DDS (Data Distribution Service) is an IoT standard for real-time, scalable and high-performance machine-to-machine communication. It was developed by the Object Management Group (OMG).

You can deploy DDS both in low-footprint devices and in the cloud.

The DDS standard has two main layers:

Data-Centric Publish-Subscribe (DCPS), which delivers the information to subscribers
Data-Local Reconstruction Layer (DLRL), which provides an interface to DCPS functionalities


**AMQP**


AMQP (Advanced Message Queuing Protocol) is an application layer protocol for message-oriented middleware environments. It is approved as an international standard.

The processing chain of the protocol includes three components that follow certain rules.

Exchange — gets messages and puts them in the queues
Message queue — stores messages until they can be safely processed by the client app
Binding — states the relationship between the first and the second components
Bluetooth


**Bluetooth**


Bluetooth is a short-range communications technology integrated into most smartphones and mobile devices, which is a major advantage for personal products, particularly wearables.

Bluetooth is well-known to mobile users. But not long ago, the new significant protocol for IoT apps appeared — Bluetooth Low-Energy (BLE), or Bluetooth Smart. This technology is a real foundation for the IoT, as it is scalable and flexible to all market innovations. Moreover, it is designed to reduce power consumption.

Standard: Bluetooth 4.2
Frequency: 2.4GHz
Range: 50-150m (Smart/BLE)
Data Rates: 1Mbps (Smart/BLE)
Zigbee


**ZigBee 3.0** 


The Zigbee is a low-power, low data-rate wireless network used mostly in industrial settings also alliance even created the universal language for the Internet of Things — Dotdot — which makes it possible for smart objects to work securely on any network and seamlessly understand each other.

Standard: ZigBee 3.0 based on IEEE802.15.4
Frequency: 2.4GHz
Range: 10-100m
Data Rates: 250kbps
WiFi


**Wi-Fi** 

WiFi is the technology for radio wireless networking of devices. It offers fast data transfer and is able to process large amounts of data.

This is the most popular type of connectivity in LAN environments.

Standard: Based on IEEE 802.11
Frequencies: 2.4GHz and 5GHz bands
Range: Approximately 50m
Data Rates: 150-200Mbps, 600 Mbps maximum
Cellular


Cellular technology is the basis of mobile phone networks. But it is also suitable for the IoT apps that need functioning over longer distances. They can take advantage of cellular communication capabilities such as GSM, 3G, 4G (and 5G soon).

The technology is able to transfer high quantities of data, but the power consumption and the expenses are high too. Thus, it can be a perfect solution for projects that send small amounts of information.

Standard: GSM/GPRS/EDGE (2G), UMTS/HSPA (3G), LTE (4G)
Frequencies: 900/1800/1900/2100MHz
Range: 35km (GSM); 200km (HSPA)
Data Rates: 35-170kps (GPRS), 120-384kbps (EDGE), 384Kbps-2Mbps (UMTS), 600kbps-10Mbps (HSPA), 3-10Mbps (LTE)
LoRaWAN


**LoRaWAN**
 (Long Range Wide Area Network) is a protocol for wide area networks. It is designed to support huge networks (e.g. smart cities) with millions of low-power devices.

LoRaWAN can provide low-cost mobile and secure bidirectional communication in various industries.

Standard: LoRaWAN
Frequency: Various
Range: 2-5km (urban area), 15km (suburban area)
Data Rates: 0.3-50 kbps


**Converting Indusry 3.0 devices to Industry 4.0**
                       
![](https://qphs.fs.quoracdn.net/main-qimg-7c0637076b2b58a2f9d79a4c293d8f7b)      

When computers were introduced in Industry 3.0, it was disruptive thanks to the addition of an entirely new technology. Now, and into the future as Industry 4.0 unfolds, computers are connected and communicate with one another to ultimately make decisions without human involvement. A combination of cyber-physical systems, the Internet of Things and the Internet of Systems make Industry 4.0 possible and the smart factory a reality. As a result of the support of smart machines that keep getting smarter as they get access to more data, our factories will become more efficient and productive and less wasteful. Ultimately, it's the network of these machines that are digitally connected with one another and create and share information that results in the true power of Industry 4.0.





**Challenges That Hold Manufacturing Aback from Industry 4.0 Adoption**


Driving organization-wide innovation is tough. At the current stage of the Industry 4.0 development, 6 out of 10 manufacturers admit the implementation barriers to be so strong, that they managed to achieve only limited progress with their industry 4.0 initiatives during the past year.


*Among the top cited challenges are:*


- Lack of unified leadership that makes cross-unit coordination difficult within the company.

- Data ownership concerns when choosing third-party vendors for hosting and operationalizing company data.

- Lack of courage to launch the radical digitalization plan.

- Lack of in-house talent to support the development and deployment of Industry 4.0 initiatives.

- Difficulties with integrating data from various sources to enable initial connectivity.

- Lack of knowledge about technologies, vendors and IT outsourcing partners that could help execute the core initiative.

- While there’s no “one-fits-it-all” approach to bringing an Industry 4.0 program to life, there are several things every company can do to diminish those adoption barriers. The first step to becoming an Industry 4.0 company is to clearly estimate the ROI different digital solutions can generate for your business.

**Solution for 4.0**

Industry 4.0 will enable integrated and cross-disciplinary engineering throughout the
value chain and throughout product and customer life cycles.
Innovation has traditionally related predominantly to product offerings, but its
major potential lies in the areas of company structures, processes, networks and
profit models, together with customer-facing functions, such as new services
and distribution channels, new uses for a strong brand and distinctive customer
engagement. 

Empirical research shows that the share price of companies deploying more than
just two types of innovation performs better on the stock exchange – and that top
innovators deploy five or more types of innovation.

*Efficient management ofinnovation*


Successful management of innovation takes in the entire company and covers strategy,
organisation, project portfolio management and product development.
The digital transformation to industry 4.0 will make it possible to improve further the
efficiency of innovation management in all these areas.
Interactive and tailored curricula make individualised learning possible, thereby
speeding up strategic implementation and organisational development.
In project portfolio management, industry 4.0 solutions make it easier not only to
track the return on investment (ROI) in innovation but also to identify risks by using
global comparative project data for monitoring and remedial purposes. In the area
of product development, information technology can be used to speed up research
and development. This transforms the sharing of information between existing
technologies within global networks along the same lines as the ’game networks’ that
the global online gaming community use.


*Efficient life cycle management*

The digital transformation to industry 4.0 will make it possible to provide relevant
data for life cycle management at any time and from anywhere.
These data will comprise not only information and reports but also the results of big
data processing to generate relevant early indicators through the use of artificial
intelligence (AI).
AI will use global cross-checking and assess the plausibility of generating relevant
bases for decision-making supported by data. It will enable companies to understand
and meet their customers’ needs better, as well as to customise product cycles.


*Smart supply chains*


Successful companies will develop new segments on the edge of their current
business that will, in time, become central to the business.

To achieve this, companies need to develop new skills, both at individual employee
level and within the organisation as a whole. A solely top-down approach will
create resistance in the organisation, while introducing pockets of innovation within
traditional business will provoke a reaction from less engaged employees.

Industry 4.0 means getting to grips with radical new approaches to business rather
than merely making incremental improvements to established business models.

*Business model optimisation*


There will be a particular focus on new models that are tailored more closely to
individual customer needs and enable new cooperative models with business
partners. However, this will place new demands on the supply chain.
The digital transformation will create a single database, making supply chains
smarter, more transparent and more efficient at every stage, from customer needs
to delivery.
Research and development, procurement and purchasing, production and sales
functions are becoming more closely aligned as digitisation advances.
The most successful companies will use better communications to integrate suppliers
and customers’ needs into all value-creation activities.
